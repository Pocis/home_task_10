export default (ctx) => {
  ctx.softRedirect = (path) => {
    const { redirect, route } = ctx;
    if (route.path !== path) redirect(path);
  };
}