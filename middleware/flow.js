export default function ({ store, softRedirect }) {
  // If the user is not submitted personal info
  const user = store.state.form
  if (!user.info.firstName) {
    return softRedirect('/form/personal-info')
  }
}