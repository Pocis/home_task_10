export const state = () => ({
  steps: {
    1: 'Personal info',
    2: 'Membership',
    3: 'Overview'
  },
  activeStep: 0,
  form: {
    info: {
      phone: {
        type: 'Home',
        number: '',
      }
    },
    membership: ''
  }
})

export const mutations = {
  setActiveStep(state, step) {
    state.activeStep = step
  },
  setPersonalInfo(state, info) {
    state.form.info = info
  },
  setMembership(state, membership) {
    state.form.membership = membership
  }
}

export const getters = {
  activeStep: state => {
    return state.activeStep
  }
}